import { FC, ReactNode } from 'react';
import * as S from './styles';

interface ButtonProps {
  id: string;
  onClick?: () => void;
  type?: 'button' | 'submit' | 'reset';
  title?: string;
  ariaLabel?: string;
  children: ReactNode;
  disabled?: boolean;
}

const Button: FC<ButtonProps> = ({
  children,
  id,
  onClick,
  title,
  type = 'button',
  ariaLabel,
  disabled,
}) => {
  return (
    <S.Container
      id={id}
      onClick={onClick}
      type={type}
      title={title}
      aria-label={ariaLabel}
      disabled={disabled}
    >
      {children}
    </S.Container>
  );
};

export default Button;
