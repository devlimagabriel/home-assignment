import { render, fireEvent } from '@testing-library/react';
import theme from 'config/theme';
import { ThemeProvider } from 'styled-components';
import Button from '.';

describe('Header', () => {
  const setup = (disabled: boolean) => {
    const mockClick = jest.fn();
    const rendered = render(
      <ThemeProvider theme={theme}>
        <Button id="test" onClick={mockClick} disabled={disabled}>
          test
        </Button>
      </ThemeProvider>
    );

    const button = rendered.container.querySelector(
      'button'
    ) as HTMLButtonElement;

    return {
      button,
      mockClick,
      ...rendered,
    };
  };

  it('should render correctly', () => {
    const { button } = setup(false);

    expect(button).toBeInTheDocument();
  });

  it('should be disabled', () => {
    const { button, mockClick } = setup(true);

    expect(button).toBeDisabled();

    fireEvent.click(button);

    expect(mockClick).not.toBeCalled();
  });

  it('should be clickable', () => {
    const { button, mockClick } = setup(false);
    fireEvent.click(button);
    expect(mockClick).toBeCalled();
  });
});
