import styled, { css } from 'styled-components';

export const Container = styled.button`
  ${({ theme }) => css`
    padding: 18px 0;
    width: 100%;
    font-weight: 600;
    font-family: inherit;
    background-color: ${theme.colors.brandPrimary};
    color: ${theme.colors.neutralWhite};
    border: none;
    border-radius: 32px;
    cursor: pointer;
    transition: background-color 300ms ease;

    &:disabled,
    &:disabled:hover {
      background-color: ${theme.colors.blueGray100};
      cursor: not-allowed;
    }

    &:focus,
    &:hover {
      background-color: #17298b;
      outline: none;
    }
  `}
`;
