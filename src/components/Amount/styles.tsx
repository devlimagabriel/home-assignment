import styled, { css } from 'styled-components';

export const Prefix = styled.div`
  ${({ theme }) => css`
    padding: 0 8px 0 12px;
    position: absolute;
    top: 0;
    left: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;
    z-index: 2;

    @media (min-width: ${theme.breakpoints.md}) {
      padding: 0 8px 0 16px;
    }
  `}
`;

export const Container = styled.div`
  position: relative;
`;

export const Input = styled.input`
  ${({ theme }) => css`
    padding: 16px 24px 16px 44px;
    position: relative;
    width: 100%;
    background-color: ${theme.colors.neutralWhite};
    border: 1px solid ${theme.colors.blueGray50};
    border-radius: 4px;
    font-size: 1.25rem;
    font-family: ${theme.fonts.rubik};
    font-weight: 500;
    color: ${theme.colors.blueGray600};
    transition: all 300ms ease;

    @media (min-width: ${theme.breakpoints.md}) {
      padding: 13px 24px 13px 44px;
      font-size: 1.5rem;
    }

    &:focus {
      outline: none;
      border-color: ${theme.colors.blueGray600};
    }
  `}
`;
