import { render, fireEvent } from '@testing-library/react';
import theme from 'config/theme';
import { ThemeProvider } from 'styled-components';
import Amount from '.';

describe('Amount', () => {
  const setup = () => {
    const rendered = render(
      <ThemeProvider theme={theme}>
        <Amount id="test" name="test" />
      </ThemeProvider>
    );

    const input = rendered.container.querySelector('input') as HTMLInputElement;

    return {
      input,
      ...rendered,
    };
  };

  it('should have rendered', () => {
    const { input, getByAltText } = setup();
    expect(input).toBeInTheDocument();
    expect(getByAltText(/$/g)).toBeInTheDocument();
  });

  it('should format number', () => {
    const { input } = setup();
    fireEvent.change(input, { target: { value: '1000' } });
    expect(input.value).toBe('1,000');
  });

  it('should not format letter', () => {
    const { input } = setup();
    fireEvent.change(input, { target: { value: 'aaaa' } });
    expect(input.value).toBe('');
  });

  it('should not format empty string', () => {
    const { input } = setup();
    fireEvent.change(input, { target: { value: '' } });
    expect(input.value).toBe('');
  });
});
