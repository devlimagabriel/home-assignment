import { ChangeEvent, useState } from 'react';
import { formatNumber } from 'utils/formatNumber';
import * as S from './styles';

interface AmountProps {
  id: string;
  name: string;
  onChange?: (event: { target: HTMLInputElement }) => void;
  amountValue: string;
}

const Amount = ({
  id,
  name,
  onChange = () => null,
  amountValue = '',
}: AmountProps): JSX.Element => {
  const [value, setValue] = useState<string>(amountValue);

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    onChange(event);
    if (!/[0-9]/g.exec(event.target.value) && event.target.value !== '') return;
    setValue(formatNumber(event.target.value));
  };

  return (
    <S.Container>
      <S.Prefix>
        <img src="/images/icons/dollar-sign.svg" alt="$" />
      </S.Prefix>
      <S.Input
        id={id}
        name={name}
        type="text"
        onChange={handleChange}
        value={value}
        placeholder="25,000"
      />
    </S.Container>
  );
};

export default Amount;
