import styled, { css } from 'styled-components';

export const Header = styled.header`
  ${({ theme }) => css`
    padding: 1rem;
    background-color: ${theme.colors.neutralWhite};

    @media (min-width: ${theme.breakpoints.md}) {
      padding: 1.5rem 3.5rem;
    }
  `}
`;

export const Logo = styled.a`
  ${({ theme }) => css`
    display: inline-block;
    img {
      max-width: 75px;

      @media (min-width: ${theme.breakpoints.md}) {
        max-width: inherit;
      }
    }
  `}
`;
