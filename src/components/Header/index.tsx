import * as S from './styles';

const Header = (): JSX.Element => {
  return (
    <S.Header>
      <S.Logo href="/">
        <img src="/images/logoOrigin.svg" alt="Origin Logo" />
      </S.Logo>
    </S.Header>
  );
};

export default Header;
