import { useState, useRef, useCallback, useEffect } from 'react';
import * as S from './styles';
import { DateState } from 'config/date-extensions';

interface ReachDateProps {
  id: string;
  name: string;
  onChange?: (date: DateState) => void;
  dateValue: Date;
}

interface ArrowsFunctions {
  [key: string]: () => void;
}

const ReachDate = ({
  id,
  name,
  onChange,
  dateValue,
}: ReachDateProps): JSX.Element => {
  const inputRef = useRef<HTMLInputElement>(null);
  const nextRef = useRef<HTMLButtonElement>(null);
  const prevRef = useRef<HTMLButtonElement>(null);
  const currentDate = new Date(dateValue);
  const [date, setDate] = useState<DateState>(
    currentDate.getYearMonth(currentDate, currentDate.getMonth())
  );

  const handleMonthChange = (month: number): void => {
    const isGoingForward = currentDate.isGoingForward(date.full, month);

    if (isGoingForward) {
      setDate(currentDate.getYearMonth(date.full, month));
    }
  };

  useEffect(() => {
    if (onChange) onChange(date);
  }, [date.month]);

  const handleArrowPress = useCallback((event: KeyboardEvent): void => {
    const arrowKeys: ArrowsFunctions = {
      ArrowLeft: () => {
        if (prevRef.current != null) prevRef.current.click();
      },
      ArrowRight: () => {
        if (nextRef.current != null) nextRef.current.click();
      },
    };

    const arrowFunctions = arrowKeys[event.key];
    if (arrowFunctions) arrowFunctions();
  }, []);

  const handleFocus = (): void => {
    window.addEventListener('keydown', handleArrowPress);
  };

  const handleBlur = (): void => {
    window.removeEventListener('keydown', handleArrowPress);
  };

  return (
    <S.Container onFocus={handleFocus} onBlur={handleBlur}>
      <input
        ref={inputRef}
        id={id}
        name={name}
        type="hidden"
        value={JSON.stringify(date)}
      />
      <S.Button
        ref={prevRef}
        type="button"
        onClick={() => {
          handleMonthChange(date.month - 1);
        }}
      >
        <img src="/images/icons/chevron-left.svg" alt="previous" />
      </S.Button>
      <S.Value>
        <S.Month>{date.formattedMonth}</S.Month>
        <S.Year>{date.year}</S.Year>
      </S.Value>
      <S.Button
        ref={nextRef}
        type="button"
        onClick={() => {
          handleMonthChange(date.month + 1);
        }}
      >
        <img src="/images/icons/chevron-right.svg" alt="next" />
      </S.Button>
    </S.Container>
  );
};

export default ReachDate;
