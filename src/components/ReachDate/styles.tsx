import styled, { css } from 'styled-components';

export const Value = styled.p`
  margin: 0;
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: center;
`;

export const Month = styled.span`
  ${({ theme }) => css`
    display: block;
    font-size: 0.875rem;
    font-weight: 600;
    color: ${theme.colors.blueGray900};
    line-height: 1.5rem;

    @media (min-width ${theme.breakpoints.lg}) {
      font-size: 1rem;
    }
  `}
`;

export const Year = styled.span`
  ${({ theme }) => css`
    display: block;
    font-size: 0.875rem;
    color: ${theme.colors.blueGray400};
    line-heigh1.5remt @media (min-width ${theme.breakpoints.lg}) {
      font-size: 1rem;
    }
  `}
`;

export const Button = styled.button`
  padding: 1rem 0.75rem;
  display: flex;
  cursor: pointer;
  background: none;
  border: none;
  outline: none;
  transition: background-color 300ms ease;

  &:hover,
  &focus {
    background-color: ${({ theme }) => theme.colors.blueGray10};
  }
`;

export const Container = styled('div').attrs(() => ({
  tabIndex: 0,
}))`
  ${({ theme }) => css`
    position: relative;
    display: flex;
    align-items: stretch;
    justify-content: space-between;
    background-color: ${theme.colors.neutralWhite};
    border: 1px solid ${theme.colors.blueGray50};
    border-radius: 4px;
    transition: border-color 300ms ease;

    &:focus,
    &:focus-visible {
      border-color: ${theme.colors.blueGray600};
      outline: none;
    }
  `}
`;
