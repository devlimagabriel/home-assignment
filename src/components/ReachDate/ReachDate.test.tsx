import { render, fireEvent } from '@testing-library/react';
import theme from 'config/theme';
import { ThemeProvider } from 'styled-components';
import ReachDate from '.';

describe('ReachDate', () => {
  const setup = (onChange?: () => void) => {
    const rendered = render(
      <ThemeProvider theme={theme}>
        <ReachDate id="test" name="test" onChange={onChange} />
      </ThemeProvider>
    );

    const input = rendered.container.querySelector('input') as HTMLInputElement;
    const buttons = rendered.container.querySelectorAll('button');
    const prevButton = buttons[0] as HTMLButtonElement;
    const nextButton = buttons[1] as HTMLButtonElement;
    const inputValues = input.value.split(',');
    const month = Number(inputValues[0]);
    const year = Number(inputValues[1]);
    const display = rendered.container.querySelector(
      'p'
    ) as HTMLParagraphElement;
    const displayValues = display.children;
    const displayMonth = displayValues[0];
    const displayYear = displayValues[1];

    return {
      input,
      prevButton,
      nextButton,
      month,
      year,
      displayMonth,
      displayYear,
      ...rendered,
    };
  };

  const fireClickMultipleTimes = (
    click: (element: HTMLButtonElement) => void,
    element: HTMLButtonElement,
    times: number
  ) => {
    for (let i = 0; i <= times; i++) {
      click(element);
    }
  };

  it('should have rendered', () => {
    const { input, prevButton, nextButton, displayMonth, displayYear } =
      setup();
    expect(input).toBeInTheDocument();
    expect(prevButton).toBeInTheDocument();
    expect(nextButton).toBeInTheDocument();
    expect(displayMonth).toBeInTheDocument();
    expect(displayYear).toBeInTheDocument();
  });

  it('should have a default date', () => {
    const { month, year } = setup();
    expect(month).toBeTruthy();
    expect(year).toBeTruthy();
  });

  it('should buttons click work', () => {
    const { prevButton, nextButton } = setup();

    const prevMock = jest.fn();
    const nextMock = jest.fn();

    prevButton.onclick = prevMock;
    nextButton.onclick = nextMock;

    fireEvent.click(prevButton);
    fireEvent.click(nextButton);

    expect(prevMock).toBeCalled();
    expect(nextMock).toBeCalled();
  });

  it('should not go backwards', () => {
    const { prevButton, input, month } = setup();
    fireEvent.click(prevButton);
    expect(input.value.split(',').includes(month.toString())).toBeTruthy();
  });

  it('should month go backwards', () => {
    const { prevButton, nextButton, input, month } = setup();
    fireClickMultipleTimes(fireEvent.click, nextButton, 13);
    fireEvent.click(prevButton);
    expect(
      input.value.split(',').includes((month + 1).toString())
    ).toBeTruthy();
  });

  it('should month go forwards', () => {
    const { nextButton, input, month } = setup();
    fireEvent.click(nextButton);
    expect(
      input.value.split(',').includes((month + 1).toString())
    ).toBeTruthy();
  });

  it('should work with onChange prop', () => {
    const mockChange = jest.fn();
    const { nextButton } = setup(mockChange);
    fireEvent.click(nextButton);
    expect(mockChange).toBeCalled();
  });

  it('should year go backwards', () => {
    const { prevButton, nextButton, input, year } = setup();
    fireClickMultipleTimes(fireEvent.click, nextButton, 12);
    fireClickMultipleTimes(fireEvent.click, prevButton, 12);
    expect(input.value.split(',').includes(year.toString())).toBeTruthy();
  });

  it('should year go forwards', () => {
    const { nextButton, input, year } = setup();
    fireClickMultipleTimes(fireEvent.click, nextButton, 12);
    expect(input.value.split(',').includes((year + 1).toString())).toBeTruthy();
  });

  it('should focus in and out', () => {
    const { container } = setup();
    const focusMock = jest.fn();
    const blurMock = jest.fn();
    const focusableContainer = container.children[0] as HTMLDivElement;

    focusableContainer.onfocus = focusMock;
    focusableContainer.onblur = blurMock;

    focusableContainer.focus();
    expect(focusMock).toBeCalled();

    focusableContainer.blur();
    expect(blurMock).toBeCalled();
  });

  it('should year go forwards with arrow', () => {
    const { container, input, month } = setup();
    fireEvent.focusIn(container.children[0]);
    fireEvent.keyDown(container, {
      key: 'ArrowRight',
      keyCode: 39,
      which: 39,
    });
    expect(
      input.value.split(',').includes((month + 1).toString())
    ).toBeTruthy();
  });

  it('should not work with keydown', () => {
    const { prevButton, nextButton, container, input, month } = setup();
    prevButton.parentNode?.removeChild(prevButton);
    nextButton.parentNode?.removeChild(nextButton);

    fireEvent.focusIn(container.children[0]);
    fireEvent.keyDown(container, {
      key: 'ArrowRight',
      keyCode: 39,
      which: 39,
    });
    expect(input.value.split(',').includes(month.toString())).toBeTruthy();

    fireEvent.keyDown(container, {
      key: 'ArrowLeft',
      keyCode: 37,
      which: 37,
    });
    expect(input.value.split(',').includes(month.toString())).toBeTruthy();

    fireEvent.keyDown(container, {
      key: 'ArrowUp',
      keyCode: 38,
      which: 38,
    });
    expect(input.value.split(',').includes(month.toString())).toBeTruthy();
  });

  it('should getYearMonth method work without month argument', () => {
    setup();
    const dateCompared = new Date();
    const yearMonthMock = new Date();
    yearMonthMock.getYearMonth(yearMonthMock);

    expect(yearMonthMock.getTime()).toBe(dateCompared.getTime());
  });

  it('should monthDiff method work', () => {
    setup();
    const dateFrom = new Date();
    const dateTo = new Date();
    dateTo.setFullYear(dateTo.getFullYear() + 2);

    expect(dateFrom.monthDiff(dateFrom, dateTo)).toBe(24);
  });
});
