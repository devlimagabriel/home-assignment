import { render, fireEvent } from '@testing-library/react';
import theme from 'config/theme';
import { ThemeProvider } from 'styled-components';
import Header from '.';

describe('Header', () => {
  const setup = () => {
    const rendered = render(
      <ThemeProvider theme={theme}>
        <Header />
      </ThemeProvider>
    );

    const logo = rendered.container.querySelector('img') as HTMLImageElement;
    const link = rendered.container.querySelector('a') as HTMLAnchorElement;
    const header = rendered.container.querySelector(
      'header'
    ) as HTMLHeadElement;

    const mockClick = jest.fn();
    link.onclick = mockClick;

    return {
      header,
      link,
      logo,
      mockClick,
      ...rendered,
    };
  };

  it('should', () => {
    expect(true).toBe(true);
  });
});
