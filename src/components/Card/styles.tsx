import { Link } from 'react-router-dom';
import styled, { css } from 'styled-components';

export const Container = styled.div`
  ${({ theme }) => css`
    padding: 24px;
    box-shadow: 0px 8px 24px rgba(30, 42, 50, 0.08);
    border-radius: 8px;
    min-height: 248px;
    display: flex;
    flex-direction: column;
    background-color: ${theme.colors.neutralWhite};
  `}
`;

export const Title = styled.p`
  ${({ theme }) => css`
    margin: 8px 0;
    font-size: 16px;
    font-weight: 600;
    line-height: 24px;
  `}
`;

export const Info = styled.div`
  ${({ theme }) => css`
    margin-bottom: 16px;
    width: 100%;
    padding: 12px 16px;
    border: 1px solid ${theme.colors.blueGray50};
    border-radius: 8px;
    display: flex;
    justify-content: space-evenly;
    gap: 16px;
  `}
`;

export const InfoTitle = styled.p`
  ${({ theme }) => css`
    margin: 0 0 2px;
    color: ${theme.colors.blueGray600};
    font-size: 12px;
  `}
`;

export const Value = styled.p<{ highlighted?: boolean }>`
  ${({ theme, highlighted }) => css`
    margin: 0;
    font-size: 16px;
    font-weight: 600;
    color: ${highlighted ? theme.colors.brandSecondary : null};
  `}
`;

export const Wrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const Button = styled(Link)<{ edit: boolean }>`
  ${({ theme, edit }) => css`
    padding: 10px 0;
    width: 100%;
    display: block;
    font-weight: 600;
    font-family: inherit;
    background-color: ${edit
      ? theme.colors.blueGray10
      : theme.colors.brandPrimary};
    color: ${edit ? theme.colors.brandSecondary : theme.colors.neutralWhite};
    border: none;
    border-radius: ${edit ? '8px' : '32px'};
    text-align: center;
    text-decoration: none;
    cursor: pointer;
    transition: background-color 300ms ease;

    &:disabled,
    &:disabled:hover {
      background-color: ${theme.colors.blueGray100};
      cursor: not-allowed;
    }

    &:focus,
    &:hover {
      background-color: ${edit ? theme.colors.blueGray50 : '#17298b'};
      outline: none;
    }
  `}
`;
