import { formatNumber } from 'utils/formatNumber';
import * as S from './styles';

interface CardProps {
  image: string;
  title: string;
  goal: string;
}

const Card = ({ image, title, goal }: CardProps): JSX.Element => {
  const savingGoalValue = window.localStorage.getItem(goal);
  const parsedSavingGoal = !!savingGoalValue && JSON.parse(savingGoalValue);
  const parsedDate = parsedSavingGoal.date && JSON.parse(parsedSavingGoal.date);

  return (
    <S.Container>
      <S.Wrapper>
        <img src={image} alt={`${title} Icon`} />
        <S.Title>{title}</S.Title>
        {!!parsedSavingGoal && (
          <S.Info>
            <div>
              <S.InfoTitle>Goal amount</S.InfoTitle>
              <S.Value highlighted>
                &#36;{formatNumber(parsedSavingGoal.amount)}
              </S.Value>
            </div>
            <div>
              <S.InfoTitle>Reach goal by</S.InfoTitle>
              <S.Value>{`${parsedDate.formattedMonth} ${parsedDate.year}`}</S.Value>
            </div>
          </S.Info>
        )}
      </S.Wrapper>
      <S.Button edit={parsedSavingGoal} to={`/goal/${goal}`}>
        {parsedSavingGoal ? 'EDIT GOAL' : 'Setup goal'}
      </S.Button>
    </S.Container>
  );
};

export default Card;
