import {
  BrowserRouter as Router,
  Switch,
  Route,
  useParams,
} from 'react-router-dom';
import DatePrototypeSetup from 'config/date-extensions';

import { Header } from 'components/shared';
import { Home, Goal } from 'screens';

function App(): JSX.Element {
  DatePrototypeSetup();
  return (
    <>
      <Header />
      <Router>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/goal/:goal">
            <Goal />
          </Route>
        </Switch>
      </Router>
    </>
  );
}

export default App;
