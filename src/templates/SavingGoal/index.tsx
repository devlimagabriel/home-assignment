import { FC, FormEvent, useEffect, useState } from 'react';

import { Amount, Button, ReachDate } from 'components/shared';
import * as S from './styles';
import { formatNumber } from 'utils/formatNumber';
import { useHistory } from 'react-router-dom';

interface SavingGoalProps {
  goal?: string;
}

const SavingGoal = ({ goal = 'savingGoal' }: SavingGoalProps) => {
  const history = useHistory();

  const savingGoalValue = window.localStorage.getItem(goal);
  const parsedSavingGoal = !!savingGoalValue && JSON.parse(savingGoalValue);
  const parsedDate = parsedSavingGoal.date && JSON.parse(parsedSavingGoal.date);

  const [amount, setAmount] = useState<number>(
    Number(parsedSavingGoal.amount?.replace(/[,]/g, '')) || 0
  );
  const today = new Date();
  const [date, setDate] = useState<{
    full: Date;
    month: number;
    year: number;
    formattedMonth: string;
  }>(today.getYearMonth(today, today.getMonth()));

  const [monthDiff, setMonthDiff] = useState<number>(0);
  const [monthlyAmount, setMonthlyAmount] = useState<number>(0);

  useEffect(() => {
    const diff = today.monthDiff(today, date.full);
    setMonthDiff(diff);
    setMonthlyAmount(amount > 0 && diff > 0 ? amount / diff : 0);
  }, [date.month, amount]);

  function handleSubmit(event: any) {
    event.preventDefault();
    const { savingGoalAmount, savingGoalReachDate } = event.target.elements;

    const savingGoalValue = {
      amount: savingGoalAmount.value,
      date: savingGoalReachDate.value,
    };

    window.localStorage.setItem(goal, JSON.stringify(savingGoalValue));
    history.push('/');
  }

  return (
    <S.Container>
      <S.Title>
        {`Let's plan your`} <b>saving goal</b>.
      </S.Title>
      <S.Form onSubmit={handleSubmit}>
        <S.Header>
          <img src="/images/icons/BuyHouseIcon.svg" alt="Buy a House Icon" />
          <div>
            <S.HeaderTitle>Buy a house</S.HeaderTitle>
            <S.HeaderSubtitle>Saving goal</S.HeaderSubtitle>
          </div>
        </S.Header>

        <S.InputWrapper>
          <S.AmountWrapper>
            <S.Label>Total amount</S.Label>
            <Amount
              amountValue={parsedSavingGoal?.amount}
              id="savingGoalAmount"
              name="savingGoalAmount"
              onChange={(event) =>
                setAmount(parseFloat(event.target.value.replaceAll(',', '')))
              }
            />
          </S.AmountWrapper>
          <S.DateWrapper>
            <S.Label>Reach goal by</S.Label>
            <ReachDate
              dateValue={parsedDate?.full || today}
              id="savingGoalReachDate"
              name="savingGoalReachDate"
              onChange={(date) => {
                setDate({
                  full: date.full,
                  month: date.month,
                  year: date.year,
                  formattedMonth: date.formattedMonth,
                });
              }}
            />
          </S.DateWrapper>
        </S.InputWrapper>

        <S.Simulation>
          <S.MonthlyAmount>
            <S.MonthlyAmountTitle>Monthly amount</S.MonthlyAmountTitle>
            <S.MonthlyAmountValue>
              &#36;{formatNumber(monthlyAmount.toString())}
            </S.MonthlyAmountValue>
          </S.MonthlyAmount>
          <S.Info>
            You’re planning <b>{monthDiff} monthly deposits</b> to reach your
            <b> &#36;{amount} </b>
            goal by
            <b>{` ${date.formattedMonth} ${date.year}`}</b>.
          </S.Info>
        </S.Simulation>
        <S.ButtonWrapper>
          <Button id="savingGoalConfirm" type="submit" disabled={!amount}>
            Confirm
          </Button>
        </S.ButtonWrapper>
      </S.Form>
    </S.Container>
  );
};

export default SavingGoal;
