import styled, { css } from 'styled-components';

export const Container = styled.section`
  margin: 0 auto;
  max-width: 560px;
`;

export const Title = styled.h2`
  ${({ theme }) => css`
    margin-top: 2rem;
    margin-bottom: 1.5rem;
    color: ${theme.colors.brandPrimary};
    font-size: 1.125rem;
    font-weight: 400;
    text-align: center;

    @media (min-width: ${theme.breakpoints.md}) {
      margin-top: 3rem;
      font-size: 1.25rem;
    }

    b {
      font-weight: 600;
    }
  `}
`;

export const Form = styled.form`
  ${({ theme }) => css`
    padding: 24px 24px 40px;
    width: 100%;
    background-color: ${theme.colors.neutralWhite};
    border-radius: 8px;
    box-shadow: 0px 16px 32px rgba(30, 42, 50, 0.08);

    @media (min-width: ${theme.breakpoints.md}) {
      padding: 32px 40px 40px;
    }
  `}
`;

export const Header = styled.div`
  ${({ theme }) => css`
    margin-bottom: 1rem;
    display: flex;
    align-items: center;
    gap: 1rem;
    @media (min-width: ${theme.breakpoints.lg}) {
      margin-bottom: 1.5rem;
    }
  `}
`;

export const HeaderTitle = styled.h3`
  ${({ theme }) => css`
    margin: 0 0 4px;
    font-family: ${theme.fonts.rubik};
    font-weight: 500;
    color: ${theme.colors.blueGray900};
    font-size: 1.25rem;
    line-height: 1.2em;

    @media (min-width: ${theme.breakpoints.lg}) {
      font-size: 1.5rem;
    }
  `}
`;

export const HeaderSubtitle = styled.p`
  ${({ theme }) => css`
    margin: 0;
    color: ${theme.colors.blueGray400};
    font-size: 0.875rem;
    line-height: 1.5em;
    @media (min-width: ${theme.breakpoints.lg}) {
      font-size: 1rem;
    }
  `}
`;

export const Label = styled.label`
  ${({ theme }) => css`
    margin-bottom: 4px;
    font-size: 0.75rem;
    line-height: 1.5em;
    font-family: inherit;
    color: ${theme.colors.blueGray900};
    @media (min-width: ${theme.breakpoints.md}) {
      font-size: 0.875rem;
    }
  `}
`;

export const InputWrapper = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;
    gap: 1rem;
    @media (min-width: ${theme.breakpoints.sm}) {
      flex-direction: row;
    }
  `}
`;

const fieldsetReset = css`
  padding: 0;
  margin: 0;
  border: none;
`;

export const AmountWrapper = styled.fieldset`
  flex: 1;
  ${fieldsetReset}
`;

export const DateWrapper = styled.fieldset`
  flex: 0.65;
  ${fieldsetReset}
`;

export const ButtonWrapper = styled.div`
  ${({ theme }) => css`
    margin-top: 2rem;

    @media (min-width: ${theme.breakpoints.sm}) {
      max-width: 320px;
      margin-left: auto;
      margin-right: auto;
    }
  `}
`;

export const Simulation = styled.div`
  ${({ theme }) => css`
    margin-top: 1.5rem;
    border: 1px solid ${theme.colors.blueGray50};
    border-radius: 8px;
    overflow: hidden;
  `}
`;

export const MonthlyAmount = styled.div`
  ${({ theme }) => css`
    padding: 1.5rem;
    display: flex;
    align-items: center;
    justify-content: space-between;
    background-color: ${theme.colors.neutralWhite};

    @media (min-width: ${theme.breakpoints.md}) {
      padding: 1.5rem 2rem 1rem;
    }
  `}
`;

export const MonthlyAmountTitle = styled.h4`
  ${({ theme }) => css`
    margin: 0;
    font-size: 1.125rem;
    font-weight: 400;
    @media (min-width: ${theme.breakpoints.md}) {
      font-size: 1.25rem;
    }
  `}
`;

export const MonthlyAmountValue = styled.p`
  ${({ theme }) => css`
    margin: 0;
    font-family: ${theme.fonts.rubik};
    font-size: 1.5rem;
    font-weight: 500;
    color: ${theme.colors.brandSecondary};

    @media (min-width: ${theme.breakpoints.md}) {
      font-size: 2rem;
    }
  `}
`;

export const Info = styled.p`
  ${({ theme }) => css`
    margin: 0;
    padding: 1.5rem 2rem;
    background-color: ${theme.colors.blueGray10};
    font-size: 0.75rem;
    line-height: 1.3em;
    text-align: center;
    @media (min-width: ${theme.breakpoints.md}) {
      text-align: initial;
    }

    b {
      font-weight: 600;
    }
  `}
`;
