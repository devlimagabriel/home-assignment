import { formatNumber } from '.';

describe('formatNumber', () => {
  it('should format with string', () => {
    expect(formatNumber('1000')).toBe('1,000');
  });

  it('should return empty string', () => {
    expect(formatNumber('')).toBe('');
  });
});
