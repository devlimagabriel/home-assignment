export const formatNumber = (number: string): string => {
  if (number === '') return '';

  const formatter = new Intl.NumberFormat('en-US');
  return formatter.format(
    parseFloat(parseFloat(number.replace(/[,]/g, '')).toFixed(2))
  );
};
