export interface DateState {
  full: Date;
  year: number;
  month: number;
  formattedMonth: string;
  time: number;
}

declare global {
  interface Date {
    getYearMonth(current: Date, newMonth?: number): DateState;
    isGoingForward(current: Date, newMonth: number): boolean;
    monthDiff(dateFrom: Date, dateTo: Date): number;
  }
}

Date.prototype.getYearMonth = (current, newMonth = 0) => {
  const newDate = new Date(current);
  newDate.setMonth(newMonth);

  return {
    full: newDate,
    time: newDate.getTime(),
    year: newDate.getFullYear(),
    month: newDate.getMonth(),
    formattedMonth: newDate.toLocaleString('en-GB', { month: 'long' }),
  };
};

Date.prototype.isGoingForward = (current, newMonth) => {
  const today = new Date();
  const newDate = new Date(current);
  newDate.setMonth(newMonth);
  const isGreaterThan = newDate.getTime() - today.getTime() > 0;
  const isTheSame =
    newDate.getMonth() == today.getMonth() &&
    newDate.getFullYear() == today.getFullYear();

  return isGreaterThan || isTheSame;
};

Date.prototype.monthDiff = (dateFrom, dateTo) => {
  return (
    dateTo.getMonth() -
    dateFrom.getMonth() +
    12 * (dateTo.getFullYear() - dateFrom.getFullYear())
  );
};

export default function DatePrototypeSetup(): boolean {
  return true;
}

export {};
