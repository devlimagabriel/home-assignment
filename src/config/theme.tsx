import { DefaultTheme } from 'styled-components';

const theme: DefaultTheme = {
  colors: {
    blueGray10: '#F4F8FA',
    blueGray50: '#E9EEF2',
    blueGray100: '#CBD5DC',
    blueGray300: '#8A9CA9',
    blueGray400: '#708797',
    blueGray600: '#4D6475',
    blueGray900: '#1E2A32',
    neutralWhite: '#FFFFFF',
    brandPrimary: '#1B31A8',
    brandSecondary: '#0079FF',
  },
  fonts: {
    rubik: "'Rubik', sans-serif",
    worksans: "'Work Sans', sans-serif",
  },
  breakpoints: {
    xs: '375px',
    sm: '480px',
    md: '768px',
    lg: '1024px',
    xl: '1200px',
    xxl: '1400px',
  },
};

export default theme;
