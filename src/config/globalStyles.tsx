import { createGlobalStyle, css } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  ${({ theme }) => css`
    * {
      box-sizing: border-box;
      font-family: ${theme.fonts.worksans};
      @media (prefers-reduced-motion) {
        animation: none;
        transition: none;
      }
    }
    body {
      margin: 0;
      padding: 0;
      background: ${theme.colors.blueGray10};
    }
  `}
`;

export default GlobalStyle;
