import SavingGoal from 'templates/SavingGoal';
import { useParams } from 'react-router-dom';

function Goal(): JSX.Element {
  const { goal } = useParams<{ goal?: string }>();
  return <SavingGoal goal={goal} />;
}

export default Goal;
