import styled, { css } from 'styled-components';

export const Container = styled.section`
  margin: 0 auto;
  padding: 0 16px;
  max-width: 1136px;
`;

export const Title = styled.h2`
  ${({ theme }) => css`
    color: ${theme.colors.blueGray900};
    font-family: ${theme.fonts.rubik};
    font-weight: 500;
    font-size: 24px;

    @media (min-width: ${theme.breakpoints.lg}) {
      font-size: 32px;
    }
  `}
`;

export const CardContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(270px, 1fr));
  gap: 16px;
`;
