import { Link } from 'react-router-dom';
import * as S from './styles';
import { Card } from 'components/shared';

function Home(): JSX.Element {
  return (
    <S.Container>
      <S.Title>Here are your savings goals!</S.Title>
      <S.CardContainer>
        <Card
          goal="college"
          title="Go to college"
          image="/images/icons/CollegeIcon.svg"
        />
        <Card
          goal="vacation"
          title="Take a vacation"
          image="/images/icons/CollegeIcon.svg"
        />
        <Card
          goal="car"
          title="Buy a car"
          image="/images/icons/CollegeIcon.svg"
        />
        <Card
          goal="wedding"
          title="Throw a wedding party"
          image="/images/icons/BuyHouseIcon.svg"
        />
        <Card
          goal="emergency"
          title="Build an emergency fund"
          image="/images/icons/CollegeIcon.svg"
        />
        <Card
          goal="baby"
          title="Have a baby"
          image="/images/icons/CollegeIcon.svg"
        />
      </S.CardContainer>
    </S.Container>
  );
}

export default Home;
