import 'styled-components';

declare module 'styled-components' {
  export interface DefaultTheme {
    colors: {
      blueGray10: string;
      blueGray50: string;
      blueGray100: string;
      blueGray300: string;
      blueGray400: string;
      blueGray600: string;
      blueGray900: string;
      neutralWhite: string;
      brandPrimary: string;
      brandSecondary: string;
    };
    fonts: {
      rubik: string;
      worksans: string;
    };
    breakpoints: {
      xs: string;
      sm: string;
      md: string;
      lg: string;
      xl: string;
      xxl: string;
    };
  }
}
