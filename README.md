# Origin - Frontend Take-Home Assignment

Created with React using CRA with typescript, tests were made with Jest and React Testing Library.

You can access the live project on [https://gabriel-home-assignment.vercel.app/](https://gabriel-home-assignment.vercel.app/)

## how to use

Recommended to use Node LTS.

clone the repository with https or SSH

```bash
# with https
git clone https://devlimagabriel@bitbucket.org/devlimagabriel/home-assignment.git

# with SSH
git clone git@bitbucket.org:devlimagabriel/home-assignment.git
```

now you can access the folder with:

```bash
cd home-assignment
```

then you need to install the dependencies

```bash
npm install
# or
yarn install
```

now you can start the project with the following command:

```bash
 npm start
 # or
 yarn start
```

Now you can access on http://localhost:3000

To run the tests you need to run the following command:

```bash
 npm test
 # or
 yarn test
```

## considerations

I've created a shared.ts file on components folder to export the global/generic components that could be used anywhere.

About the template, I thought about creating separate components for form and simulation, but I created as a template and I thought would be unnecessary doing that, because the component is already generic enough and small, maybe if there was more resposabilities I believe would be good to split some parts of the code.

If you guys have any opinion or questions about the project you can reach me on my email devlimagabriel@gmail.com or [Linkedin](https://www.linkedin.com/in/gbrl/)
